var a = [3, -6, 8, -9, -4, 5, 12];
console.log(a);

// Câu 1: Tính tổng các số trong mảng a
// 1. Đầu vào: cho sẵn mảng a
// 2. Đầu ra: tổng các số trong mảng
/** 3. Giải thuật:
 *     Bước 1: Khai báo tiến tích lũy tổng
 *     Bước 2: Duyệt vòng lặp (forr i, for of)
 *     Bước 3: Tích lũy cho biến tổng
 *
 */

var total = 0;
for (let i = 0; i < a.length; i++) {
  total += a[i];
}
// for (const item of a) {
//     total += item
// }
console.log(total); // total = 9

// Câu 2: Tìm phần tử âm lớn nhất trong mảng a. Xuất giá trị và chỉ số tại vị trí đó
/**
 * TH1: Mảng toàn số dương: a = [4, 9, 1, 12, 12, 9, 2, 18]; => Mảng không có phần tử âm nào
 * TH2: Mảng toàn số âm a = [-4, -9, -1, -12, -12, -9, -2, -18]; => Kỹ thuật tìm lớn nhất
 * TH3: Mảng vừa có số âm vừa có số dương: a = [3, -6, 8, -9, -4, 5, 12];
 * TH4: 
 * Giải thuật:
 * 1. Đầu vào: Mảng các số
 * 2. Đầu ra: TÌm chỉ số và giá trị của số âm lớn nhất trong mảng
 * 3. Giải thuật
 * Bước 1: Đặt biến chỉ số âm đầu tiên chiSoAmDauTien = -1; // Chưa tìm thấy số âm đầu tiên
 * Bước 2: Duyệt vòng lặp, nếu tìm thấy số âm, gán chiSoAmDauTienlà chỉ số âm hiện tại đang duyệt. Dừng vòng lặp
 * Bước 3: Duyệt lại mảng từ chiSoAmDauTien + 1 trở đi
 * Bước 4; Nếu phần tử tiếp theo là số âm và số âm này lớn hơn giá trị tại phần tử chiSoAmDauTien => cập nhật lại chiSoAmDauTien là chỉ số hiện tại
 * Bước 5: Khai báo mảng chứa các phần từ âm lớn nhất bằng nhau
 * Bước 6: Duyệt lại mảng từ chiSoAmDauTien trở đi
 * Bước 7: Kiểm tra phần tử có giá trị bằng phần tử tại chiSoAmDauTien thì đưa vào mảng các giá trị âm lớn nhất
 *
 */

let chiSoAmDauTien = -1;
for(var i = 0; i < a.length; i++) {
    if(a[i] < 0) {
        chiSoAmDauTien = i;
        break; // Nếu có vòng lặp lồng nhau thì nó thoát khỏi vòng lặp hiện tại và thực hiện vòng lặp kế tiếp
    }
}

var chiSoAmMax;
if(chiSoAmDauTien != -1) {
    for(var i = chiSoAmDauTien + 1; i < a.length; i++) {
        if(a[i] < 0 && a[i] > a(chiSoAmDauTien)) {

        }
    }
} else {
    console.log("Mảng không có số âm!!")
}
